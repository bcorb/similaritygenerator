﻿using SimilarityGenerator.ViewModel;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Command class for opening a dialog box
    /// </summary>
    class OpenCommand : ICommand
    {
        #region Class variables
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the OpenCommand class.
        /// </summary>
        /// <param name="viewModel">ViewModel that is being bound</param>
        public OpenCommand(AlignmentViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event System.EventHandler CanExecuteChanged {
            add 
            {
                CommandManager.RequerySuggested += value;
            }
            remove 
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Determines whether the command can be executed
        /// This command can always be executed
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        /// <returns>Whether the command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Execution logic of the command
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter)
        {
            viewModel.OpenFile();
            viewModel.ImportSequences();
        }
        #endregion
    }
}
