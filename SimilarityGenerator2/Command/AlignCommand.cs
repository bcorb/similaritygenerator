﻿using SimilarityGenerator.ViewModel;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Command used for performing the alignment
    /// </summary>
    class AlignCommand : ICommand
    {
        #region Class variables
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of the AlignCommand class
        /// </summary>
        /// <param name="viewModel">ViewModel that is being bound</param>
        public AlignCommand(AlignmentViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Determines whether the command can be executed
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        /// <returns>Whether the command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return viewModel.SequencesImported && viewModel.Validate;
        }

        /// <summary>
        /// Execution logic of the command
        /// No logic is executed as it is handled by the Background worker
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter) { }
        #endregion
    }
}
