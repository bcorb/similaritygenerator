﻿using SimilarityGenerator.ViewModel;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Command used for exiting the program
    /// </summary>
    class ExitCommand : ICommand
    {       
        #region Class variables
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of the ExitCommand class
        /// </summary>
        /// <param name="viewModel">ViewModel that is being bound</param>
        public ExitCommand(AlignmentViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Determines whether the command can be executed
        /// Always can execute
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        /// <returns>Whether the command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Execution logic of the command
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter)
        {
            viewModel.Exit();
        }
        #endregion
    
    }
}
