﻿using SimilarityGenerator.ViewModel;
using System;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// RelayCommand class. Responsible for initiating the 
    /// Background worker thread
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Class variables
        readonly Action<object> execute;
        readonly Predicate<object> canExecute;
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new command that can always execute
        /// </summary>
        /// <param name="execute">The execution logic</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null, null) { }

        /// <summary>
        /// Creates a new command
        /// </summary>
        /// <param name="execute">The execution logic</param>
        /// <param name="canExecute">The execution status logic</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute, 
            AlignmentViewModel viewModel)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            this.viewModel = viewModel;
            this.execute = execute;
            this.canExecute = canExecute;
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter) 
                && viewModel.SequencesImported && viewModel.Validate;
        }

        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Execution logic of the command
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter)
        {
            execute(parameter);
        }
        #endregion
    }
}
