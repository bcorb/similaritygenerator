﻿using SimilarityGenerator.ViewModel;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Command for the export of data via excel file
    /// </summary>
    class ExportExcelCommand : ICommand
    {
        #region Class variables
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the ExportExcelCommand class
        /// </summary>
        /// <param name="viewModel">ViewModel that is being bound</param>
        public ExportExcelCommand(AlignmentViewModel viewModel)
        {
            this.viewModel = viewModel; 
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Determines whether the command can be executed
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        /// <returns>Whether the command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return viewModel.SequenceCount();
        }

        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Execution logic of the command
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter)
        {
            viewModel.ExportExcel();
        }
        #endregion
    }
}
