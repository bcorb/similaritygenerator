﻿using SimilarityGenerator.ViewModel;
using System.Windows.Input;

namespace SimilarityGenerator.Command
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Command class for enabling and disabling controls on the GUI
    /// </summary>
    class UserControlsCommand : ICommand
    {
        #region Class variables
        private AlignmentViewModel viewModel;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of UserControlCommand
        /// </summary>
        /// <param name="viewModel">ViewModel that is being bound</param>
        public UserControlsCommand(AlignmentViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Registers can execute change event
        /// Always executes
        /// </summary>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Registers can execute change event
        /// </summary>
        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Execution logic of the command
        /// </summary>
        /// <param name="parameter">Object that is bound</param>
        public void Execute(object parameter)
        {
            viewModel.UseLengthMum();
        }
        #endregion
    }
}
