﻿using Bio;
using Bio.IO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Imports a list of sequences from valid fasta formatted file
    /// </summary>
    public class Import
    {
        #region Class variables
        private String fileName;
        private List<ISequence> sequences;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the import class
        /// </summary>
        /// <param name="fileName">Name of the file that is being imported</param>
        public Import(String fileName)
        {
            sequences = new List<ISequence>();
            this.fileName = fileName;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether 
        /// the file import has been successful
        /// </summary>
        public bool ImportSuccessful { get; private set; }

        /// <summary>
        /// Gets or sets a list of sequences from the import
        /// </summary>
        public List<ISequence> Sequences
        {
            get
            {
                return sequences;
            }
            set
            {
                sequences = value;
                NumberSequences = sequences.Count;
            }
        }

        /// <summary>
        /// Gets or sets the number of sequnces in the list
        /// </summary>
        public int NumberSequences { get; set; }

        #endregion

        #region Public methods
        /// <summary>
        /// Loads sequence objects directly from file
        /// </summary>
        public void LoadFromFile()
        {
            try
            {
                ISequenceParser parser = new Bio.IO.FastA.FastAParser();

                // Parse the file          
                using (parser.Open(fileName))
                {
                    Sequences = parser.Parse().ToList();
                }

                CheckSequences();
            }
            catch (Exception e)
            {
                ImportSuccessful = false;
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Validates sequence import
        /// </summary>
        private void CheckSequences()
        {
            if (Sequences != null)
            {
                ImportSuccessful = true;
            }
            else
            {
                ImportSuccessful = false;
            }
        }
        #endregion
    }
}
