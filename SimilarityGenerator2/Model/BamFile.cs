﻿using Bio.IO.BAM;
using Bio.IO.SAM;
using System.Collections.Generic;
using System.IO;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Creation and export of BAM file which contains aligned data
    /// and normalised alignment score
    /// </summary>
    public class BamFile
    {
        #region Class variables
        private BAMFormatter bam;
        private SAMAlignmentHeader samHeader;
        private List<Edge> sequences;
        private string fileLocation;
        #endregion

        #region Constants
        private const string BAM_FILE_NAME = "\\newBamFile.bam";
        private const string ALIGMENT_SCORE_TAG = "AS";
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the BamFile class
        /// </summary>
        public BamFile(List<Edge> sequences, string fileLocation)
        {
            this.sequences = sequences;
            this.fileLocation = fileLocation;

            bam = new BAMFormatter();
            samHeader = new SAMAlignmentHeader();
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Calls the methods to create a SAM header file, create
        /// a BAM file, and save it
        /// </summary>
        public void OutputBamFile()
        {
            CreateSamHeader();
            CreateBamFile();
        }
        #endregion

        #region Private variables
        /// <summary>
        /// Adds each sequence to the SAM header with the score
        /// defined as an optional SAM tag
        /// </summary>
        private void CreateSamHeader()
        {
            foreach (Edge sequence in sequences)
            {
                samHeader.ReferenceSequences.Add(new ReferenceSequenceInfo(sequence.Source.SequenceId,
                                                 sequence.Source.Sequence.Count));
                SAMRecordField record = new SAMRecordField();
                record.Tags.Add(new SAMRecordFieldTag(ALIGMENT_SCORE_TAG, sequence.Weight.ToString()));
                samHeader.RecordFields.Add(record);
            }
            CreateBamFile();
        }

        /// <summary>
        /// Adds SAM header to BAM file and outputs BAM file
        /// </summary>
        private void CreateBamFile()
        {
            using (Stream stream = File.OpenWrite(fileLocation + BAM_FILE_NAME))
            {
                bam.WriteHeader(samHeader, stream);
            }
        }
        #endregion
    }
}
