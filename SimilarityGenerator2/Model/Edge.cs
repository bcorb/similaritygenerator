﻿namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Represents the similarity score between sequences (Vertex objects)
    /// Contains information on score, destination and source
    /// </summary>
    public class Edge
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of the Edge class
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="weight"></param>
        public Edge(Vertex source, Vertex destination, double weight)
        {
            this.Source = source;
            this.Destination = destination;
            this.Weight = weight;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the source vertex that represents a sequence
        /// </summary>
        public Vertex Source { get; set; }

        /// <summary>
        /// Gets and sets the destination vertex that represnts a sequence
        /// </summary>
        public Vertex Destination { get; set; }

        /// <summary>
        /// Gets and sets the alignment score
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// String output of source for GUI display
        /// </summary>
        public string SourceString { get { return Source.SequenceId; } }

        /// <summary>
        /// String output of destination for GUI display
        /// </summary>
        public string SourceDestination { get { return Destination.SequenceId; } }

        #endregion

        #region Overriden methods
        /// <summary>
        /// Overrides Equals methods for use by hashset
        /// Enables comparison of hashset edge objects by ID
        /// </summary>
        /// <param name="obj">Object that is being compared</param>
        /// <returns>Boolean as to whether object exists</returns>
        public override bool Equals(object obj)
        {
            var item = obj as Edge;
            if ((object)obj == null)
            {
                return false;
            }

            return (Source.SequenceId == item.Source.SequenceId) && 
                (Destination.SequenceId == item.Destination.SequenceId);
        }

        /// <summary>
        /// Overrides GetHashCode method for use in hashset comparison
        /// Enables comparison of hashset edge objects by ID
        /// </summary>
        /// <returns>Hashcode</returns>
        public override int GetHashCode()
        {
            return this.Source.SequenceId.GetHashCode() + this.Destination.SequenceId.GetHashCode();
        }
        #endregion
    }
}

