﻿using Bio;
using Bio.Algorithms.Alignment;
using Bio.SimilarityMatrices;
using System;
using System.Collections.Generic;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Provides alignment data and calculations for the Nucmer aligner
    /// </summary>
    public class Nucmer : Similarity
    {
        #region Class variables
        private NucmerPairwiseAligner aligner;
        private int lengthMum;
        #endregion

        #region Constants
        private const int SAME_SEQUENCE_SCORE = 1;
        private const int MIN_MUM_LENGTH = 1;
        private const int DIAGONAL_WEIGHT = 1;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the Nucmer.cs class
        /// </summary>
        /// <param name="fileLocation">Input file</param>
        /// <param name="sequenceList">Imported sequences</param>
        /// <param name="gapCost">User parameter</param>
        /// <param name="gapExtensionCost">User parameter</param>
        /// <param name="resolver">User parameter</param>
        /// <param name="simMatrix">User parameter</param>
        /// <param name="lengthMum">User parameter</param>
        public Nucmer(string fileLocation, List<ISequence> sequenceList, int gapCost, 
            int gapExtensionCost, IConsensusResolver resolver, 
            SimilarityMatrix.StandardSimilarityMatrix simMatrix, int lengthMum) :
            base(fileLocation, sequenceList, gapCost, gapExtensionCost, resolver, simMatrix)
        {
            if (lengthMum < MIN_MUM_LENGTH)
            {
                Failed = true;
            }

            aligner = new NucmerPairwiseAligner();
            this.lengthMum = lengthMum;
        }
        #endregion

        #region Overriden methods
        /// <summary>
        /// Performs the alignment for the Nucmer aligner
        /// </summary>
        public override void Align()
        {
            try
            {
                int sourceCount = DEFAULT_COUNT;
                foreach (ISequence sourceSeq in SequenceList)
                {
                    int destinationCount = DEFAULT_COUNT;
                    var result = aligner.Align(sourceSeq, SequenceList);
                    foreach (IPairwiseSequenceAlignment pairwise in result)
                    {
                        foreach (PairwiseAlignedSequence pas in pairwise)
                        {
                            destinationCount++;
                            Edge edge = new Edge(
                            new Vertex(sourceSeq, sourceSeq.ID, sourceCount),
                            new Vertex(pas.SecondSequence, pas.SecondSequence.ID, destinationCount), 
                            pas.Score);

                            Edges.Add(edge);
                        }
                    }
                    //adds in diagonal edge as nucmer does not compare to itself
                    Edges.Add(new Edge(
                            new Vertex(sourceSeq, sourceSeq.ID, sourceCount),
                            new Vertex(sourceSeq, sourceSeq.ID, DEFAULT_COUNT), DIAGONAL_WEIGHT));

                    sourceCount++;
                }
            }
            catch (Exception e)
            {
                Failed = true;
                ExceptionMessage = "Alignment failed!";
            }
        }

        /// <summary>
        /// Changes the Nucmer defaults based on user parameters
        /// </summary>
        public override void SetAlignmentParameters()
        {
            aligner.ConsensusResolver = resolver;
            aligner.GapOpenCost = gapCost;
            aligner.GapExtensionCost = gapExtensionCost;
            aligner.SimilarityMatrix = new SimilarityMatrix(simMatrix);
            aligner.LengthOfMUM = lengthMum;
        }
        #endregion
    }
}
