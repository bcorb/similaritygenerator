﻿using Bio;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Represents a sequence in the similarity scoring processs
    /// </summary>
    public class Vertex
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of the Vertex class
        /// </summary>
        /// <param name="sequence">Source sequence</param>
        /// <param name="sequenceId">Sequence count</param>
        /// <param name="vertexId">Unique identifier</param>
        public Vertex(ISequence sequence, string sequenceId, int vertexId)
        {
            this.Sequence = sequence;
            this.SequenceId = sequenceId;
            this.VertexId = vertexId;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets and sets the sequence of the Vertex
        /// </summary>
        public ISequence Sequence { get; set; }

        /// <summary>
        /// Gets and sets the sequence ID
        /// </summary>
        public string SequenceId { get; set; }

        /// <summary>
        /// Gets and sets the vertex ID
        /// </summary>
        public int VertexId { get; set; }
        #endregion
    }
}
