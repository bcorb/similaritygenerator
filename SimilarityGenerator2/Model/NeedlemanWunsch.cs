﻿using Bio;
using Bio.Algorithms.Alignment;
using Bio.SimilarityMatrices;
using System;
using System.Collections.Generic;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Provides alignment data and calculations for the NeedlmanWunsch aligner
    /// </summary>
    public class NeedlemanWunsch : Similarity
    {
        #region Class variables
        private NeedlemanWunschAligner aligner;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the NeedlemanWuncsh.cs class
        /// </summary>
        /// <param name="fileLocation">Imported file</param>
        /// <param name="sequenceList">List of imported sequences</param>
        /// <param name="gapCost">User parameter</param>
        /// <param name="gapExtensionCost">User parameter</param>
        /// <param name="resolver">User parameter</param>
        /// <param name="simMatrix">User parameter</param>
        public NeedlemanWunsch(string fileLocation, List<ISequence> sequenceList, int gapCost, 
            int gapExtensionCost, IConsensusResolver resolver, 
            SimilarityMatrix.StandardSimilarityMatrix simMatrix) :
            base(fileLocation, sequenceList, gapCost, gapExtensionCost, resolver, simMatrix)
        {
            aligner = new NeedlemanWunschAligner();
        }
        #endregion

        #region Overidden methods
        /// <summary>
        /// Performs alignment based on NeedleWunsch
        /// Adds similarity scores to edges in graph
        /// </summary>
        public override void Align()
        {
            try
            {
                int sourceCount = DEFAULT_COUNT;
                foreach (ISequence sourceSequence in SequenceList)
                {
                    int destinationCount = DEFAULT_COUNT;

                    foreach (ISequence sequence in SequenceList)
                    {
                        Edge edge = new Edge(
                            new Vertex(sourceSequence, sourceSequence.ID, sourceCount),
                            new Vertex(sequence, sequence.ID, destinationCount), DEFAULT_SCORE);
                        Edge reverse = new Edge(
                            new Vertex(sequence, sequence.ID, destinationCount),
                            new Vertex(sourceSequence, sourceSequence.ID, sourceCount), DEFAULT_SCORE);

                        if (!Edges.Contains(edge))
                        {
                            var result = aligner.Align(sourceSequence, sequence);

                            foreach (IPairwiseSequenceAlignment pairwise in result)
                            {
                                foreach (PairwiseAlignedSequence pas in 
                                    pairwise.PairwiseAlignedSequences)
                                {
                                    edge.Weight = pas.Score;
                                    reverse.Weight = pas.Score;
                                }
                            }
                            Edges.Add(edge);
                            Edges.Add(reverse);
                        }

                        destinationCount++;
                    }
                    sourceCount++;
                }
            }
            catch (Exception e)
            {
                Failed = true;
                ExceptionMessage = "Alignment failed!";
            }
        }

        /// <summary>
        /// Changes the NeedlmanWunsch defaults based on user parameters
        /// </summary>
        public override void SetAlignmentParameters()
        {
            aligner.ConsensusResolver = resolver;
            aligner.GapOpenCost = gapCost;
            aligner.GapExtensionCost = gapExtensionCost;
            aligner.SimilarityMatrix = new SimilarityMatrix(simMatrix);
        }
        #endregion
    }
}
