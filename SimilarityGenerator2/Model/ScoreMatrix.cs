﻿using Excel = Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace SimilarityGenerator.Model
{   
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Outputs the data of all similarity scores generated
    /// Primarilty reponsible for the export of data via excel and text file
    /// </summary>
    public class ScoreMatrix
    {
        #region Class variables
        private int numberSequences;
        private List<List<long>> alignmentScores;
        private List<Edge> sortedGraphEdges;
        private string fileLocation;
        private Excel.Application excel;
        private Excel._Worksheet worksheet;
        #endregion

        #region Constants
        private const int START_ROW = 1;
        private const int START_INDEX = 1;
        private const int DEFAULT_COUNT = 0;
        private const int SET_ROW = 2;
        private const string ROW_SOURCE_SEQ_TITLE = "Source sequence";
        private const string ROW_DEST_SEQ_TITLE = "Destination sequence";
        private const string ROW_SCORE_TITLE = "Alignment score";
        private const string WORKSHEET_NAME = "Similarity Matrix";
        private const string TEXT_FILE_NAME = "\\scoreOutput";
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the ScoreMatrix class
        /// This constructor to be used when exporting text file
        /// </summary>
        /// <param name="numberSequences">Count of sequences</param>
        /// <param name="fileLocation">Location of save file, used for text file</param>
        /// <param name="edge">List of edges</param>
        public ScoreMatrix(int numberSequences, string fileLocation, List<Edge> edge)
        {
            this.numberSequences = numberSequences;
            this.fileLocation = fileLocation;

            alignmentScores = new List<List<long>>();
            GraphEdges = new List<Edge>(edge);
            sortedGraphEdges = new List<Edge>();

            SortEdges();
        }

        /// <summary>
        /// Creates a new instance of the ScoreMatrix class
        /// This constructor to be used when exporting excel file
        /// </summary>
        /// <param name="numberSequences">Count of sequences</param>
        /// <param name="edge">List of edges</param>
        public ScoreMatrix(int numberSequences, List<Edge> edge)
        {
            this.numberSequences = numberSequences;

            alignmentScores = new List<List<long>>();
            GraphEdges = new List<Edge>(edge);
            sortedGraphEdges = new List<Edge>();

            SortEdges();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets and sets list of graph edges
        /// </summary>
        public List<Edge> GraphEdges { get; set; }

        /// <summary>
        /// Gets and sets whether file has been created 
        /// </summary>
        public bool VerifyFileCreation { get; set; }
        #endregion

        #region Private methods
        /// <summary>
        /// Sorts the list of edges by vertex ID
        /// </summary>
        private void SortEdges()
        {
            sortedGraphEdges = GraphEdges.OrderBy(o => o.Destination.VertexId).ToList();
            sortedGraphEdges = GraphEdges.OrderBy(o => o.Source.VertexId).ToList();
        }

        /// <summary>
        /// Extracts ID from sequence ID string
        /// </summary>
        /// <param name="id">Sequence ID data</param>
        /// <returns>ID number or original string if ID is not present</returns>
        private string FilterId(string id)
        {
            int start = id.IndexOf("|") + START_INDEX;
            int end = id.IndexOf("|ref", start);

            if (end != -START_INDEX)
            {
                return id.Substring(start, end - start);
            }
            else
            {
                return id;
            }
        }

        /// <summary>
        /// Adds scores to the excel file matrix
        /// </summary>
        private void AddScoresGrid()
        {
            var row = START_ROW;
            for (int i = DEFAULT_COUNT; i < sortedGraphEdges.Count; i++)
            {
                //change row
                if (i % numberSequences == DEFAULT_COUNT)
                {
                    row++;
                }
                worksheet.Cells[(i % numberSequences) + SET_ROW, row] = sortedGraphEdges[i].Weight;
            }
        }

        /// <summary>
        /// Adds the border to the excel matrix
        /// </summary>
        private void SetGridBorder()
        {
            var count = START_ROW;
            for (int i = numberSequences; i >= START_INDEX; i--)
            {
                count++;
                worksheet.Cells[count, 1] = FilterId(sortedGraphEdges[i].Destination.SequenceId);
                worksheet.Cells[1, count] = FilterId(sortedGraphEdges[i].Destination.SequenceId);
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Writes similarity data to text file
        /// </summary>
        public void OutputTxtFile()
        {
            try
            {
                using (StreamWriter file = new StreamWriter(fileLocation + TEXT_FILE_NAME, true))
                {
                    foreach (Edge edge in sortedGraphEdges)
                    {
                        file.WriteLine(FilterId(edge.Source.SequenceId) + " " 
                            + FilterId(edge.Destination.SequenceId) + " " + edge.Weight);
                    }
                }

                VerifyFileCreation = File.Exists(fileLocation + TEXT_FILE_NAME);
            }
            catch (Exception e) { }
        }

        /// <summary>
        /// Writes similarity data to excel file
        /// </summary>
        public void OutputExcelFile()
        {
            excel = new Excel.Application();
            excel.Workbooks.Add();

            worksheet = (Excel.Worksheet)excel.ActiveSheet;
            worksheet.Name = WORKSHEET_NAME;

            SetGridBorder();            
            AddScoresGrid();

            //excel doc is hidden until document is populated
            excel.Visible = true;
        }       
        #endregion
    }
}
