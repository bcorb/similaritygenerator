﻿using Bio;
using Bio.Algorithms.Alignment;
using Bio.Algorithms.MUMmer;
using Bio.SimilarityMatrices;
using System;
using System.Collections.Generic;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Provides alignment data and calculations for the Mummer aligner
    /// </summary>
    public class Mummer : Similarity
    {
        #region Constants
        private const int SAME_SEQUENCE_SCORE = 1;
        private const int MIN_MUM_LENGTH = 1;
        private const int DIAGONAL_WEIGHT = 1;
        private const string MUM_LENGTH_ERROR = "Minimum length of MUM cannot be less than 1.";
        private const string ALIGNER_ERROR = "Imported not supported by Mummer aligner";
        #endregion

        #region Class variables
        private MUMmerAligner aligner;
        private int lengthMum;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the Mummer.cs class
        /// </summary>
        /// <param name="fileLocation">Imported file</param>
        /// <param name="sequenceList">List of imported sequences</param>
        /// <param name="gapCost">User parameter</param>
        /// <param name="gapExtensionCost">User parameter</param>
        /// <param name="resolver">User parameter</param>
        /// <param name="simMatrix">User parameter</param>
        /// <param name="lengthMum">User parameter</param>
        public Mummer(string fileLocation, List<ISequence> sequenceList, int gapCost, 
            int gapExtensionCost, IConsensusResolver resolver, 
            SimilarityMatrix.StandardSimilarityMatrix simMatrix, int lengthMum) :
            base(fileLocation, sequenceList, gapCost, gapExtensionCost, resolver, simMatrix)
        {
            if (lengthMum < MIN_MUM_LENGTH)
            {
                Failed = true;
                ExceptionMessage = MUM_LENGTH_ERROR;
            }

            aligner = new MUMmerAligner();
            this.lengthMum = lengthMum;

            if (!IsValid())
            {
                Failed = true;
                ExceptionMessage = ALIGNER_ERROR;
            }
        }
        #endregion

        #region Overriden methods
        /// <summary>
        /// Peforms that actual alignment with mummer
        /// </summary>
        /// <param name="aligner">Aligner to be used</param>
        public override void Align()
        {
            try
            {
                int sourceCount = DEFAULT_COUNT;
                foreach (ISequence sourceSeq in SequenceList)
                {
                    int destinationCount = DEFAULT_COUNT;
                    var result = aligner.Align(sourceSeq, SequenceList);
                    foreach (IPairwiseSequenceAlignment pairwise in result)
                    {
                        foreach (PairwiseAlignedSequence pas in pairwise)
                        {
                            destinationCount++;
                            Edge edge = new Edge(
                            new Vertex(sourceSeq, sourceSeq.ID, sourceCount),
                            new Vertex(pas.SecondSequence, pas.SecondSequence.ID, destinationCount), 
                            pas.Score);

                            Edges.Add(edge);
                        }
                    }
                    //adds in diagonal edge as mummer does not compare to itself
                    Edges.Add(new Edge(
                            new Vertex(sourceSeq, sourceSeq.ID, sourceCount),
                            new Vertex(sourceSeq, sourceSeq.ID, DEFAULT_COUNT), DIAGONAL_WEIGHT));

                    sourceCount++;
                }
            }
            catch (Exception e)
            {
                Failed = true;
                ExceptionMessage = "Alignment failed!";
            }
        }

        /// <summary>
        /// Changes the Mummer defaults based on user parameters
        /// </summary>
        /// <param name="aligner">Aligner to be used</param>
        public override void SetAlignmentParameters()
        {
            aligner.ConsensusResolver = resolver;
            aligner.GapOpenCost = gapCost;
            aligner.GapExtensionCost = gapExtensionCost;
            aligner.SimilarityMatrix = new SimilarityMatrix(simMatrix);
            aligner.LengthOfMUM = lengthMum;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Checks whether sequence list contains valid DNA or RNA sequences for mummer
        /// </summary>
        /// <returns>Sequence is valid if true</returns>
        private bool IsValid()
        {
            foreach (ISequence sequence in SequenceList)
            {
                if (sequence.Alphabet.Name == "Dna" || sequence.Alphabet.Name == "Rna")
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
