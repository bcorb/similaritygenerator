﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Responsible for normalising score data
    /// </summary>
    public class Normalise
    {
        #region Constants
        private const int DIAGONAL_VALUE = 1;
        private const double DEFAULT_HIGHEST = 0;
        private const double DEFAULT_LOWEST = 1000000;
        private const int DEFAULT_COUNT = 0;
        #endregion

        #region Class variables
        private List<Edge> edges;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the Normalise class
        /// </summary>
        /// <param name="edges">List of edges to be normalised</param>
        public Normalise(List<Edge> edges)
        {
            this.edges = edges;
            Time = new Stopwatch();
        }
        #endregion

        #region Properties
        public Stopwatch Time { get; private set; }
        #endregion

        #region Public methods
        /// <summary>
        /// Performs normalisation
        /// Returns normalised data
        /// </summary>
        /// <returns>List of normalised edges</returns>
        public List<Edge> GetNormalisedScores()
        {
            StartTime();

            ManageNegativeValues();
            PerformNormalisation();

            StopTime();

            return edges;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Start the stopwatch for debugging
        /// </summary>
        private void StartTime()
        {
            Time.Start();
        }

        /// <summary>
        /// Stops time for debugging
        /// </summary>
        private void StopTime()
        {
            Time.Stop();
            TimeSpan ts = Time.Elapsed;
            DebugTime(ts.ToString());
        }

        /// <summary>
        /// Debugs time taken to perform the normalisation
        /// </summary>
        /// <param name="time"></param>
        private void DebugTime(string time)
        {
            using (StreamWriter file = new StreamWriter("debug.txt", true))
            {
                file.WriteLine("Normalisation: " + time);
            }
        }

        /// <summary>
        /// Performs the calculation to normalise data
        /// </summary>
        private void PerformNormalisation()
        {
            for (int i = DEFAULT_COUNT; i < edges.Count; i++)
            {
                //sets diagonal weight
                if (edges[i].Destination.SequenceId == edges[i].Source.SequenceId)
                {
                    edges[i].Weight = DIAGONAL_VALUE;
                }
                else
                {
                    List<Edge> row = GetRowValues(edges[i].Source.VertexId);
                    List<Edge> column = GetColValues(edges[i].Destination.VertexId);

                    List<Edge> total = new List<Edge>(row);
                    total.AddRange(column);

                    double lowest = DEFAULT_LOWEST;
                    double highest = DEFAULT_HIGHEST;

                    //finds highest and lowest values
                    for (int j = DEFAULT_COUNT; j < total.Count; j++)
                    {
                        if (total[j].Weight < lowest)
                        {
                            lowest = total[j].Weight;
                        }

                        if (total[j].Weight > highest)
                        {
                            highest = total[j].Weight;
                        }
                    }

                    //min-max noramalisation formula
                    edges[i].Weight = (edges[i].Weight - lowest) / (highest - lowest);
                    edges[i + 1].Weight = edges[i].Weight;
                    i++;
                }
            }   
        }

        /// <summary>
        /// Gets the highest value of a given row
        /// </summary>
        /// <param name="rowNumber">Number of the row</param>
        /// <returns>Highest value of the given row</returns>
        private List<Edge> GetRowValues(int rowNumber)
        {
            //finds all elements that are in same col
            return edges.FindAll(item => item.Source.VertexId == rowNumber);
        }

        /// <summary>
        /// Gets highest value of given column
        /// </summary>
        /// <param name="colNumeber">Number of the column</param>
        /// <returns>Highest value of the given column</returns>
        private List<Edge> GetColValues(int colNumber)
        {
            //finds all elements that are in the same row
            return edges.FindAll(item => item.Destination.VertexId == colNumber);
        }

        /// <summary>
        /// Adjusts negative values to positive values before normalisation is performed
        /// </summary>
        private void ManageNegativeValues()
        {
            double lowestNegative = DEFAULT_COUNT;
            double addition = DEFAULT_COUNT;

            //finds lowest negative value
            for (int i = DEFAULT_COUNT; i < edges.Count; i++)
            {
                if (edges[i].Weight < lowestNegative)
                {
                    lowestNegative = edges[i].Weight;
                    addition -= lowestNegative + 1;
                }
            }

            //adjusts each score by value of lowest negative minus zero
            for (int i = DEFAULT_COUNT; i < edges.Count; i++)
            {
                edges[i].Weight = edges[i].Weight + addition;
            }
        }
        #endregion
    }
}
