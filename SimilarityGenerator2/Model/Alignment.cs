﻿using Bio;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace SimilarityGenerator.Model
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Holds data relating to the alignment including user parameters and results
    /// Calls classes to perform the alignment
    /// Bound to values in the view tp display data in GUI and output formats
    /// </summary>
    public class Alignment : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Class variables
        private ObservableCollection<Edge> scores;
        private List<ISequence> sequenceList;
        private int gapcost;
        private int gapExtensionCost;
        private IConsensusResolver resolver;
        private double resolverDouble;
        private int lengthMum;
        private String fileLocation;
        private Aligner selectedAligner;
        private List<Edge> normalisedEdges;
        private int numberOfSequences;
        private List<String> sourceID;
        private List<String> destinationID;
        private List<double> score;
        private String alignerString;
        private SimilarityMatrix selectedMatrix;
        private bool validate;
        private Visibility failed;
        private int currentProgress;
        private Visibility progresVisibility;
        private bool mummerFields;
        private string exceptionMessage;
 
        private Similarity similarityAligner;
        #endregion

        #region Constants

        private const int DEFAULT_GAP_COST = -1;
        private const int DEFAULT_EXT_COST = -1;
        private const int DEFAULT_RESOLVER = -1;
        private const int DEFAULT_LENGHT_MUM = 10;
        private const int MIN_VALUE = 0;
        private const int MIN_MUM = 1;
        private const string GAP_COST_POS_ERROR = "Gapcost must be negative";
        private const string GAP_EXT_POS_ERROR = "Gap extension cost must be negative";
        private const string MUMMER_NEG_ERROR = "Length of Mum cannot be less than 1";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the Alignment class
        /// Sets default values from GUI
        /// </summary>
        public Alignment() 
        {
            Gapcost = DEFAULT_GAP_COST;
            GapExtensionCost = DEFAULT_EXT_COST;
            ResolverDouble = DEFAULT_RESOLVER;
            LengthMum = DEFAULT_LENGHT_MUM;

            Failed = Visibility.Hidden;
            SelectedAligner = Aligner.Mummer;
            Validate = false;
        }
        #endregion

        #region Public methods        
        /// <summary>
        /// Picks the appropriate aligner based on the user selection
        /// Calls the calculation method
        /// </summary>
        public void PeformAlignment()
        {
            //remove any error messages from previous alignment attempt
            Failed = Visibility.Hidden;

            switch (SelectedAligner)
            {
                case Aligner.Mummer:
                    similarityAligner = new Mummer(FileLocation, SequenceList, Gapcost, 
                        GapExtensionCost, resolver, SimMatrix, LengthMum);
                    break;

                case Aligner.NeedlemanWunsch:
                    similarityAligner = new NeedlemanWunsch(FileLocation, SequenceList, Gapcost, 
                        GapExtensionCost, resolver, SimMatrix);
                    break;

                case Aligner.Nucmer:
                    similarityAligner = new Nucmer(FileLocation, SequenceList, Gapcost, 
                        GapExtensionCost, resolver, SimMatrix, LengthMum);
                    break;

                case Aligner.SmithWaterman:
                    similarityAligner = new SmithWaterman(FileLocation, SequenceList, Gapcost, 
                        GapExtensionCost, resolver, SimMatrix);
                    break;
            }

            SetScores();
        }
        #endregion

        #region Properties

        #region Aligner
        /// <summary>
        /// Enum value of possible aligners
        /// </summary>
        public enum Aligner { Mummer, SmithWaterman, NeedlemanWunsch, Nucmer };

        /// <summary>
        /// Gets or set enum value indicating the aligner selected by the user
        /// Sets mummerFields based on the selection
        /// </summary>
        public Aligner SelectedAligner
        {
            get
            {
                return selectedAligner;
            }
            set
            {
                selectedAligner = value;

                //enables or disables fields for mummer and nucmer use
                if (value == Aligner.Mummer || value == Aligner.Nucmer)
                {
                    MummerFields = true;
                }
                else
                {
                    MummerFields = false;
                }
                OnPropertyChanged("Aligner");
            }
        }

        /// <summary>
        /// Gets and sets string value indicating
        /// </summary>
        public String AlignerString
        {
            get
            {
                return alignerString;
            }
            set
            {
                alignerString = value;
                OnPropertyChanged("AlignerString");
            }
        }

        #endregion

        #region SimilarityMatrix

        /// <summary>
        /// Gets and sets value indicating whether the mummer user paramters
        /// should be visible
        /// </summary>
        public bool MummerFields
        {
            get
            {
                return mummerFields;
            }
            set
            {
                mummerFields = value;
                OnPropertyChanged("MummerFields");
            }
        }

        /// <summary>
        /// Enum for the similarity matrix selection by the user
        /// </summary>
        public enum SimilarityMatrix
        {
            Blosum45,
            Blosum50,
            Blosum62,
            Blosum80,
            Blosum90,
            Pam250,
            Pam30,
            Pam70,
            AmbiguousDna,
            AmbiguousRna,
            DiagonalScoreMatrix,
            eDnaFull,
        };

        /// <summary>
        /// Gets and sets the progress bar visibility for the GUI
        /// </summary>
        public Visibility ProgressVisibility
        {
            get
            {
                return progresVisibility;
            }
            set
            {
                progresVisibility = value;
                OnPropertyChanged("ProgressVisibility");
            }
        }

        /// <summary>
        /// Gets and sets the similarity matrix for the alignment
        /// </summary>
        public SimilarityMatrix SelectedMatrix
        {
            get
            {
                return selectedMatrix;
            }
            set
            {
                selectedMatrix = value;
                SetMatrix();
                OnPropertyChanged("Aligner");
            }
        }

        /// <summary>
        /// Gets and sets a similarity matrix object
        /// </summary>
        public Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix SimMatrix { get; set; }

        /// <summary>
        /// Gets or sets the value used to by the progress bar
        /// </summary>
        public int CurrentProgress
        {
            get { return this.currentProgress; }
            set
            {
                if (this.currentProgress != value)
                {
                    this.currentProgress = value;
                    this.OnPropertyChanged("CurrentProgress");
                }
            }
        }
        #endregion

        #region Metadata
        /// <summary>
        /// Gets and sets the count of imported sequences
        /// This is bound to the GUI count
        /// </summary>
        public int NumberOfSequences
        {
            get
            {
                return numberOfSequences;
            }
            set
            {
                numberOfSequences = value;
                OnPropertyChanged("NumberOfSequences");
            }
        }

        /// <summary>
        /// Gets and sets the list of imported sequences
        /// </summary>
        public List<ISequence> SequenceList
        {
            get
            {
                return sequenceList;
            }
            set
            {
                sequenceList = value;
                NumberOfSequences = value.Count;
                OnPropertyChanged("SequenceList");
            }
        }

        /// <summary>
        /// Gets and set the collection property used by the datagrid in the GUI
        /// </summary>
        public ObservableCollection<Edge> Scores
        {
            get
            {
                return scores;
            }

            set
            {
                scores = value;
                OnPropertyChanged("Scores");
            }
        }

        /// <summary>
        /// Gets and sets the user paramter for file location
        /// </summary>
        public String FileLocation
        {
            get
            {
                return fileLocation;
            }
            set
            {
                fileLocation = value;
                OnPropertyChanged("FileLocation");
            }
        }

        /// <summary>
        /// Gets and sets the aligned/normalised edges list
        /// </summary>
        public List<Edge> NormalisedEdges
        {
            get
            {
                return normalisedEdges;
            }
            set
            {
                normalisedEdges = value;

                //set sequenceID, score, weight
                List<String> sourceID = new List<String>();
                List<String> destinationID = new List<String>();
                List<double> score = new List<double>();

                foreach (Edge edge in value)
                {
                    sourceID.Add(edge.Source.SequenceId);
                    destinationID.Add(edge.Source.SequenceId);
                    score.Add(edge.Weight);
                }

                SourceID = sourceID;
                DestinationID = destinationID;
                Score = score;

                OnPropertyChanged("NormalisedEdges");
            }
        }

        /// <summary>
        /// Gets and sets the string ID for the source sequence
        /// </summary>
        public List<String> SourceID
        {
            get
            {
                return sourceID;
            }
            set
            {
                sourceID = value;
                OnPropertyChanged("SourceID");
            }
        }

        /// <summary>
        /// Gets and sets the string ID for the destination sequence
        /// </summary>
        public List<String> DestinationID
        {
            get
            {
                return destinationID;
            }
            set
            {
                destinationID = value;
                OnPropertyChanged("SourceID");
            }
        }

        /// <summary>
        /// Gets and sets the string ID for the list of scores
        /// </summary>
        public List<double> Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
                OnPropertyChanged("Score");
            }
        }

        #endregion

        #region User parameters
        public int LengthMum
        {
            get
            {
                return lengthMum;
            }
            
            set
            {
                lengthMum = value;
                OnPropertyChanged("LengthMum");
            }
        }

        public int Gapcost
        {
            get
            {
                return gapcost;
            }
            set
            {
                gapcost = value;
                OnPropertyChanged("Gapcost");
            }
        }

        public int GapExtensionCost
        {
            get
            {
                return gapExtensionCost;
            }

            set
            {
                gapExtensionCost = value;
                OnPropertyChanged("GapExtensionCost");
            }
        }

        public double ResolverDouble
        {
            get
            {
                return resolverDouble;
            }

            set
            {
                resolverDouble = value;
                resolver = new SimpleConsensusResolver(value);
                OnPropertyChanged("ResolverDouble");
            }
        }

        #endregion

        /// <summary>
        /// Gets boolean indicated whether the alignment hit an exception
        /// Used to bind to GUI error message text field
        /// </summary>
        public Visibility Failed
        {
            get
            {
                return failed;
            }
            set
            {
                failed = value;
                OnPropertyChanged("Failed");
            }
        }

        /// <summary>
        /// Gets or sets any exception message generated
        /// </summary>
        public string ExceptionMessage
        {
            get
            {
                return exceptionMessage;
            }
            set
            {
                exceptionMessage = value;
                OnPropertyChanged("ExceptionMessage");
            }
        }

        /// <summary>
        /// Gets and sets boolean for validation errors present
        /// </summary>
        public bool Validate 
        {
            get
            {
                return validate;
            }
            set
            {
                validate = value;
                OnPropertyChanged("Validate");
            }
        }
        #endregion

        #region IPropertyNotify implementation
        /// <summary>
        /// Property change event listener
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Handles the property change event for bound properties
        /// </summary>
        /// <param name="propertyName">Name of property that has changed</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion   
    
        #region IDataErrorInfo implementation
        /// <summary>
        /// IDataError implementation requirement
        /// </summary>
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Provides customer error messages
        /// </summary>
        /// <param name="columnName">Value being checked</param>
        /// <returns>Error message</returns>
        public string this[string columnName]
        {
            get 
            {
                string result = string.Empty;

                switch (columnName)
                {
                    case "Gapcost":
                        if (Gapcost >= MIN_VALUE)
                        {
                            result = GAP_COST_POS_ERROR;
                        } 
                        break;
                    case "GapExtensionCost":
                        if (GapExtensionCost >= MIN_VALUE)
                        {
                            result = GAP_EXT_POS_ERROR;
                        }
                        break;
                    case "LengthMum":
                        if (LengthMum < MIN_MUM)
                        {
                            result = MUMMER_NEG_ERROR;
                        }
                        break;
                }

                //checks for empty value
                if (result == String.Empty)
                {
                    Validate = true;
                }
                else
                {
                    Validate = false;
                }
                
                return result;
            }
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Sets the matrix based on user selection
        /// </summary>
        private void SetMatrix()
        {
            switch (SelectedMatrix)
            {
                case SimilarityMatrix.AmbiguousDna:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        AmbiguousDna;
                    break;

                case SimilarityMatrix.AmbiguousRna:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        AmbiguousRna;
                    break;

                case SimilarityMatrix.Blosum45:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Blosum45;
                    break;

                case SimilarityMatrix.Blosum50:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Blosum50;
                    break;

                case SimilarityMatrix.Blosum62:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Blosum62;
                    break;

                case SimilarityMatrix.Blosum80:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Blosum80;
                    break;

                case SimilarityMatrix.Blosum90:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Blosum90;
                    break;

                case SimilarityMatrix.DiagonalScoreMatrix:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        DiagonalScoreMatrix;
                    break;

                case SimilarityMatrix.eDnaFull:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        EDnaFull;
                    break;

                case SimilarityMatrix.Pam250:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Pam250;
                    break;

                case SimilarityMatrix.Pam30:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Pam30;
                    break;

                case SimilarityMatrix.Pam70:
                    SimMatrix = Bio.SimilarityMatrices.SimilarityMatrix.StandardSimilarityMatrix.
                        Pam70;
                    break;                   
            }
        }

        /// <summary>
        /// Gets normalised data and set the score field for GUI
        /// Manages any exception messages from the alignment
        /// </summary>
        private void SetScores()
        {
            if (!similarityAligner.Failed)
            {
                while (!similarityAligner.Complete)
                {
                    similarityAligner.CalculateScore();

                    //Sets values for bound GUI properties
                    NormalisedEdges = similarityAligner.NormalisedEdges;
                    CurrentProgress = similarityAligner.CompletedSequences;
                    Scores = new ObservableCollection<Edge>(NormalisedEdges);
                }
            }
            else
            {
                //shows error message text on GUI
                Failed = Visibility.Visible;
                ExceptionMessage = similarityAligner.ExceptionMessage;
            }
        }
        #endregion
    }
}
