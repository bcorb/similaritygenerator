﻿using SimilarityGenerator.ViewModel;
using System.Windows;

namespace SimilarityGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Creates a new instance of MainWindow
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new AlignmentViewModel();
        }
    }
}
