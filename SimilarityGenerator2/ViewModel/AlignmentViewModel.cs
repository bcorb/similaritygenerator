﻿using SimilarityGenerator.Command;
using SimilarityGenerator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace SimilarityGenerator.ViewModel
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// View Model for the Alignment.cs Model class
    /// Handles commands from the GUI and calls required methods from the model
    /// </summary>
    public class AlignmentViewModel
    {
        #region Class variables
        private Alignment score;
        private Import import;
        private BamFile bam;
        private ScoreMatrix matrix;
        private FolderBrowserDialog folder;
        private readonly BackgroundWorker worker;
        private readonly ICommand instigateWorkCommand;
        private Stopwatch time;
        #endregion

        #region Constants
        private const int NO_SEQUENCES = 0;
        private const string SUCCESSFUL_EXPORT = "Data successfully exported";
        private const string UNSUCCESSFUL_EXPORT = "Cannot export data";
        private const string FAA_FILTER = "FAA files|*.faa";
        private const string DEBUG_FILE = "debug.txt";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new ScoreViewModel instance
        /// </summary>
        public AlignmentViewModel()
        {
            //Background worker initialisation
            this.instigateWorkCommand = new RelayCommand(o => this.worker.RunWorkerAsync(),
                o => !this.worker.IsBusy, this);
            this.worker = new BackgroundWorker();
            this.worker.DoWork += this.DoWork;
            this.worker.ProgressChanged += this.ProgressChanged;

            //Alignment object initialisation
            score = new Alignment();
            score.ProgressVisibility = Visibility.Hidden;

            //initialises Command objects with view model
            OpenCommand = new OpenCommand(this);
            AlignCommand = new AlignCommand(this);
            ExportTextCommand = new ExportTextCommand(this);
            ExportExcelCommand = new ExportExcelCommand(this);
            ExportBamCommand = new ExportBamCommand(this);
            AboutCommand = new AboutCommand(this);
            ExitCommand = new ExitCommand(this);

            //creats combo box items for GUI
            SetAlignerList();
            SetMatrixList();
        }
        #endregion

        #region Background worker
        /// <summary>
        /// Command for use by Background worker
        /// </summary>
        public ICommand InstigateWorkCommand
        {
            get { return this.instigateWorkCommand; }
        }

        /// <summary>
        /// Peforms the tasks of the Background worker
        /// </summary>
        /// <param name="sender">this object</param>
        /// <param name="e">arguments</param>
        private void DoWork(object sender, DoWorkEventArgs e)
        {
            ///accesses previous background thread to clear data
            App.Current.Dispatcher.Invoke((Action)delegate()
            {
                ClearDatagrid();
            });

            GenerateScores();
        }

        

        /// <summary>
        /// Property changes event handler
        /// </summary>
        /// <param name="sender">this objsct</param>
        /// <param name="e">arguments</param>
        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            score.CurrentProgress = e.ProgressPercentage;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets and sets the list of enums used by the aligner combobox
        /// </summary>
        public List<AlignerComboBoxItems> AlignerListEnum { get; private set; }

        /// <summary>
        /// Gets and set the list of enums used by the matrix combobox
        /// </summary>
        public List<SimilarityMatrixComboBoxItems> MatrixListEnum { get; private set; }

        /// <summary>
        /// Gets the model object being used by this viewmodel
        /// </summary>
        public Alignment Score { get { return score; } }

        /// <summary>
        /// Return boolean if the aligner has been selected
        /// </summary>
        public bool AlignerSelected { get; private set; }

        /// <summary>
        /// Checks whether any sequence have been imported
        /// </summary> 
        public bool SequencesImported { get; set; }

        /// <summary>
        /// Returns boolean as to whether mummer or nucmer is selected
        /// </summary>
        public bool MummerSelected { get; private set; }

        /// <summary>
        /// Checks whether an aligner enum has been selected
        /// </summary>
        public bool CanUpdate { get { return AlignerSelected; } }

        /// <summary>
        /// Gets the OpenCommand for the ViewModel
        /// </summary>
        public ICommand OpenCommand { get; private set; }

        /// <summary>
        /// Gets the ExportTextCommand for the ViewModel
        /// </summary>
        public ICommand ExportTextCommand { get; private set; }

        /// <summary>
        /// Gets the ExportTextCommand for the ViewModel
        /// </summary>
        public ICommand ExportExcelCommand { get; private set; }

        /// <summary>
        /// Gets the AlignCommand for the ViewModel
        /// </summary>
        public ICommand AlignCommand { get; private set; }

        /// <summary>
        /// Gets the ExportBamCommand for the ViewModel
        /// </summary>
        public ICommand ExportBamCommand { get; private set; }

        /// <summary>
        /// Gets the AboutCommand for the Viewmodel
        /// </summary>
        public ICommand AboutCommand { get; private set; }

        /// <summary>
        /// Gets the ExitCommand for the Viewmodel
        /// </summary>
        public ICommand ExitCommand { get; private set; }

        /// <summary>
        /// Checks whether there are any validation errors present in the 
        /// bound values
        /// </summary>
        public bool Validate { get { return score.Validate; } }
        #endregion

        #region Internal methods
        /// <summary>
        /// Displays an open file dialog window
        /// </summary>
        internal void OpenFile()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = FAA_FILTER;

            if (dialog.ShowDialog() == true)
            {
                score.FileLocation = dialog.FileName;
            }
        }

        /// <summary>
        /// Creates a list of ISeqeunce objects from the input file
        /// </summary>
        internal void ImportSequences()
        {
            import = new Import(score.FileLocation);
            import.LoadFromFile();

            SequencesImported = import.ImportSuccessful;
            score.SequenceList = import.Sequences;

            ClearDatagrid();
        }

        /// <summary>
        /// Generates the alignment scores
        /// </summary>
        internal void GenerateScores()
        {
            //shows progress bar
            score.ProgressVisibility = Visibility.Visible;
            score.PeformAlignment();
            //hides progress bar 
            score.ProgressVisibility = Visibility.Hidden;
        }

        /// <summary>
        /// Returns true if an aligner that uses the mummer length
        /// field has been selected. Hides/shows fields in GUI based 
        /// on this value
        /// </summary>
        internal void UseLengthMum()
        {
            if (score.SelectedAligner == Alignment.Aligner.Mummer ||
                score.SelectedAligner == Alignment.Aligner.Nucmer)
            {
                MummerSelected = true;
            }
        }

        /// <summary>
        /// Performs export of data as text file
        /// </summary>
        internal void ExportTxt()
        {
            OpenFolderBrowser();

            matrix = new ScoreMatrix(score.NumberOfSequences, folder.SelectedPath, 
                score.NormalisedEdges);
            matrix.OutputTxtFile();
                        
            VerifyFileCreationMessage(matrix.VerifyFileCreation);
        }

        /// <summary>
        /// Exports data to excel file
        /// </summary>
        internal void ExportExcel()
        {
            matrix = new ScoreMatrix(score.NumberOfSequences, score.NormalisedEdges);
            Thread excel = new Thread(matrix.OutputExcelFile);
            excel.Start();
        }

        /// <summary>
        /// Checks whether any sequences have been imported
        /// </summary>
        /// <returns>True if sequence count is above zero</returns>
        internal bool SequenceCount()
        {
            return score.NumberOfSequences > NO_SEQUENCES;
        }

        /// <summary>
        /// Exports data to Bam file/object
        /// </summary>
        internal void BamExport()
        {
            OpenFolderBrowser();

            bam = new BamFile(score.NormalisedEdges, folder.SelectedPath);
            bam.OutputBamFile();

            VerifyFileCreationMessage(true);
        }

        /// <summary>
        /// Loads the 'about' menu
        /// </summary>
        internal void LaunchAbout()
        {
            System.Windows.MessageBox.Show("Similarity Generator version 1.0 \n" +
                                            "Author: Brendan Corbett");
        }

        /// <summary>
        /// Exits the program
        /// </summary>
        internal void Exit()
        {
            try
            {
                Environment.Exit(-1);
            }
            catch (SecurityException e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Start stopwatch for debugging
        /// </summary>
        private void StartTime()
        {
            time = new Stopwatch();
            time.Start();
        }

        /// <summary>
        /// Stops stopwatch for debugging
        /// </summary>
        private void StopTime()
        {
            time.Stop();
            TimeSpan ts = time.Elapsed;
            DebugTime(ts.ToString());
        }

        /// <summary>
        /// Writes time taken to file for debugging
        /// </summary>
        /// <param name="time">Time taken</param>
        private void DebugTime(string time)
        {
            using (StreamWriter file = new StreamWriter(DEBUG_FILE, true))
            {
                file.WriteLine("Aligner: " + score.SelectedAligner.ToString()
                    + " Number of sequences: " + score.NumberOfSequences + " time: " + time);
            }
        }

        /// <summary>
        /// Provides the list of enums for the SimilarityMatrix combobox binding
        /// </summary>
        private void SetMatrixList()
        {
            MatrixListEnum = new List<SimilarityMatrixComboBoxItems>()
                {
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.AmbiguousDna, ValueMatrixString 
                        = "AmbiguousDna"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.AmbiguousRna, ValueMatrixString 
                        = "AmbiguousRna"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Blosum45, ValueMatrixString 
                        = "Blosum45"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Blosum50, ValueMatrixString 
                        = "Blosum50"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Blosum62, ValueMatrixString 
                        = "Blosum62"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Blosum80, ValueMatrixString 
                        = "Blosum80"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Blosum90, ValueMatrixString 
                        = "Blosum90"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.DiagonalScoreMatrix, ValueMatrixString 
                        = "DiagonalScoreMatrix"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.eDnaFull, ValueMatrixString 
                        = "eDnaFull"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Pam250, ValueMatrixString 
                        = "Pam250"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Pam30, ValueMatrixString 
                        = "Pam30"},
                    new SimilarityMatrixComboBoxItems()
                    { ValueMatrixEnum = Alignment.SimilarityMatrix.Pam70, ValueMatrixString
                        = "Pam70"}
                };
        }

        /// <summary>
        /// Provides the list of enums for the Aligner combobox binding
        /// </summary>
        private void SetAlignerList()
        {
            AlignerListEnum = new List<AlignerComboBoxItems>()
                {
                    new AlignerComboBoxItems()
                    { ValueAlignerEnum = Alignment.Aligner.Mummer, ValueAlignerString 
                        = "Mummer" },
                    new AlignerComboBoxItems()
                    { ValueAlignerEnum = Alignment.Aligner.NeedlemanWunsch, ValueAlignerString 
                        = "Needleman Wunsch" },
                    new AlignerComboBoxItems()
                    { ValueAlignerEnum = Alignment.Aligner.Nucmer, ValueAlignerString 
                        = "Nucmer" },
                    new AlignerComboBoxItems()
                    { ValueAlignerEnum = Alignment.Aligner.SmithWaterman, ValueAlignerString 
                        = "Smith Waterman" },
                };
        }

        /// <summary>
        /// Opens a folder dialog so that the user
        /// can choose an output location
        /// </summary>
        private void OpenFolderBrowser()
        {
            folder = new FolderBrowserDialog();
            folder.ShowDialog();
        }

        /// <summary>
        /// Displays a message box informing the user if the 
        /// data has been exported
        /// </summary>
        /// <param name="created">True if the file has been sucessfully created</param>
        private void VerifyFileCreationMessage(bool created)
        {
            if (created)
            {
                System.Windows.Forms.MessageBox.Show(SUCCESSFUL_EXPORT);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show(UNSUCCESSFUL_EXPORT);
            }
        }

        /// <summary>
        /// Clears GUI datagrid
        /// </summary>
        private void ClearDatagrid()
        {
            if (score.Scores != null)
            {
                score.Scores.Clear();
            }
        }
        #endregion
    }
}
