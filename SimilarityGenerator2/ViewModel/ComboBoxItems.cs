﻿using SimilarityGenerator.Model;

namespace SimilarityGenerator.ViewModel
{
    /// <summary>
    /// Author: Brendan Corbet
    /// 
    /// Contains data for Aligner GUI combobox
    /// </summary>
    public class AlignerComboBoxItems
    {
        /// <summary>
        /// Gets and sets alignment enum
        /// </summary>
        public Alignment.Aligner ValueAlignerEnum { get; set; }

        /// <summary>
        /// Gets and set the string used in the combobox
        /// </summary>
        public string ValueAlignerString { get; set; }
    }

    /// <summary>
    /// Contains data for SimilarityMatrix GUI combobox
    /// </summary>
    public class SimilarityMatrixComboBoxItems
    {
        /// <summary>
        /// Gets and sets similarity matrix enum
        /// </summary>
        public Alignment.SimilarityMatrix ValueMatrixEnum { get; set; }

        /// <summary>
        /// Gets and set the string used in the combobox
        /// </summary>
        public string ValueMatrixString { get; set; }
    }
}
