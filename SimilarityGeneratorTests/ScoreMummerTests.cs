﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using Bio.SimilarityMatrices;
using Bio;
using System.Collections.Generic;
using System.Linq;

namespace SimilarityGeneratorTest
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Unit test cases for the Mummer.cs class
    /// </summary>
    [TestClass]
    public class ScoreMummerTests
    {
        #region Class variables
        private Mummer mummer;
        private Import import;
        private SimilarityMatrix.StandardSimilarityMatrix simMatrix;
        private SimpleConsensusResolver resolver;
        private List<Edge> edges;
        #endregion

        #region Constants
        private const int GAP_COST = -1;
        private const int ZERO_GAP_COST = 0;
        private const int POSITIVE_GAP_COST = 10;
        private const int ZERO_GAP_EXT = 0;
        private const int POSITIVE_GAP_EXT = 10;
        private const int GAP_EXT = -1;
        private const int HIGH_GAP_EXT = 50;
        private const int RESOVLER = 2;
        private const int LENGTH_MUM = 10;
        private const int ZERO_MUM = 0;
        private const int NEGATIVE_MUM = -10;
        private const String proteinFileImport = "test.faa";
        private const String dnaFileImport = "DNAtest.faa";
        private const string fileOutput = "output";
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            import = new Import(dnaFileImport);
            import.LoadFromFile();

            simMatrix = SimilarityMatrix.StandardSimilarityMatrix.AmbiguousDna;
            resolver = new SimpleConsensusResolver(RESOVLER);

            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Exception thrown when protein file is imported
        /// </summary>
        [TestMethod]
        public void ProteinFileRead()
        {
            import = new Import(proteinFileImport);
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Checks that DNA file is correctly identified
        /// </summary>
        [TestMethod]
        public void DnaFileRead()
        {
            mummer.CalculateScore();
        }

        /// <summary>
        /// Confirms edges are created after file impoert
        /// </summary>
        [TestMethod]
        public void EdgesCreated()
        {
            mummer.CalculateScore();
            Assert.IsNotNull(mummer.Edges);
        }

        /// <summary>
        /// Edges are not created when file import is incorrect
        /// </summary>
        [TestMethod]
        public void EdgesNotCreated()
        {
            import = new Import(proteinFileImport);
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.AreEqual(0, mummer.Edges.Count);
        }

        /// <summary>
        /// Exception thrown when sequence count is zero
        /// </summary>
        [TestMethod]
        public void NoSequenceException()
        {
            string incorrectFile = "wrong";

            import = new Import(incorrectFile);
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Checks that similarity scores are added to each edge
        /// </summary>
        [TestMethod]
        public void ScoresGenerated()
        {
            mummer.CalculateScore();

            edges = mummer.Edges.ToList();

            Assert.IsNotNull(edges[1].Weight);
            Assert.IsNotNull(edges[3].Weight);
            Assert.IsNotNull(edges[5].Weight);
        }

        /// <summary>
        /// Checks that source is added to each edge
        /// </summary>
        [TestMethod]
        public void SourceAdded()
        {
            mummer.CalculateScore();

            edges = mummer.Edges.ToList();

            Assert.IsNotNull(edges[1].Source);
            Assert.IsNotNull(edges[3].Source);
            Assert.IsNotNull(edges[5].Source);
        }

        /// <summary>
        /// Checks that destination is added to each edge
        /// </summary>
        [TestMethod]
        public void DestinationAdded()
        {
            mummer.CalculateScore();

            edges = mummer.Edges.ToList();

            Assert.IsNotNull(edges[1].Destination);
            Assert.IsNotNull(edges[3].Destination);
            Assert.IsNotNull(edges[5].Destination);
        }

        /// <summary>
        /// No scores present for incorrect import
        /// </summary>
        [TestMethod]
        public void ScoresNotGenerated()
        {
            import = new Import(proteinFileImport);
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            mummer.CalculateScore();

            edges = mummer.Edges.ToList();

            Assert.AreEqual(0, edges.Count);
        }

        /// <summary>
        /// Exception is thrown when gap cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapCost()
        {
            mummer = new Mummer(fileOutput, import.Sequences, ZERO_GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapCost()
        {
            mummer = new Mummer(fileOutput, import.Sequences, POSITIVE_GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapExt()
        {
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, POSITIVE_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapExt()
        {
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, ZERO_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Gap cost must be greater/equal to gap extension cost
        /// </summary>
        [TestMethod]
        public void GapCostNotGreaterThanExtCost()
        {
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, HIGH_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception thrown when mum lenght is zero
        /// </summary>
        [TestMethod]
        public void ZeroMumLength()
        {
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, ZERO_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception thrown when mum lenght is negative
        /// </summary>
        [TestMethod]
        public void NegativeMumLength()
        {
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, NEGATIVE_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Exception thrown when the similarity matrix alphabet
        /// does not match the input sequences
        /// </summary>
        [TestMethod]
        public void SimilarityMatrixNotMatchImportedAlphabet()
        {
            simMatrix = SimilarityMatrix.StandardSimilarityMatrix.AmbiguousRna;
            mummer = new Mummer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, NEGATIVE_MUM);

            Assert.IsTrue(mummer.Failed);
        }

        /// <summary>
        /// Normalised edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void NormalisedEdgesSet()
        {
            mummer.CalculateScore();
            Assert.IsNotNull(mummer.NormalisedEdges);
        }

        /// <summary>
        /// Edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void EdgesSet()
        {
            mummer.CalculateScore();
            Assert.IsTrue(mummer.Edges.Count > 0);
        }
        #endregion
    }
}
