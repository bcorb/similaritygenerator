﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using Bio.SimilarityMatrices;
using Bio;
using System.Collections.Generic;
using System.Linq;

namespace SimilarityGeneratorTest
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the SmithWaterman.cs class
    /// </summary>
    [TestClass]
    public class ScoreSmithWatermanTests
    {
        #region Class variables
        private SmithWaterman smith;
        private Import import;
        private SimilarityMatrix.StandardSimilarityMatrix simMatrix;
        private SimpleConsensusResolver resolver;
        private List<Edge> edges;
        #endregion

        #region Constants
        private const int GAP_COST = -1;
        private const int ZERO_GAP_COST = 0;
        private const int POSITIVE_GAP_COST = 10;
        private const int ZERO_GAP_EXT = 0;
        private const int POSITIVE_GAP_EXT = 10;
        private const int GAP_EXT = -1;
        private const int HIGH_GAP_EXT = 50;
        private const int RESOVLER = 2;
        private const int ZERO_MUM = 0;
        private const int NEGATIVE_MUM = -10;
        private const String proteinFileImport = "test.faa";
        private const String dnaFileImport = "DNAtest.faa";
        private const string fileOutput = "output";
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            import = new Import(dnaFileImport);
            import.LoadFromFile();

            simMatrix = SimilarityMatrix.StandardSimilarityMatrix.AmbiguousDna;
            resolver = new SimpleConsensusResolver(RESOVLER);

            smith = new SmithWaterman(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Confirms edges are created after file import
        /// </summary>
        [TestMethod]
        public void EdgesCreated()
        {
            smith.CalculateScore();
            Assert.IsNotNull(smith.Edges);
        }

        /// <summary>
        /// Exception thrown when sequence count is zero
        /// </summary>
        [TestMethod]
        public void NoSequenceException()
        {
            string incorrectFile = "wrong";

            import = new Import(incorrectFile);
            smith = new SmithWaterman(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Checks that source is added to each edge
        /// </summary>
        [TestMethod]
        public void SourceAdded()
        {
            smith.CalculateScore();

            edges = smith.Edges.ToList();

            Assert.IsNotNull(edges[1].Source);
            Assert.IsNotNull(edges[3].Source);
            Assert.IsNotNull(edges[5].Source);
        }

        /// <summary>
        /// Checks that destination is added to each edge
        /// </summary>
        [TestMethod]
        public void DestinationAdded()
        {
            smith.CalculateScore();

            edges = smith.Edges.ToList();

            Assert.IsNotNull(edges[1].Destination);
            Assert.IsNotNull(edges[3].Destination);
            Assert.IsNotNull(edges[5].Destination);
        }

        /// <summary>
        /// Exception is thrown when gap cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapCost()
        {
            smith = new SmithWaterman(fileOutput, import.Sequences, ZERO_GAP_COST, GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapCost()
        {
            smith = new SmithWaterman(fileOutput, import.Sequences, POSITIVE_GAP_COST, GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapExt()
        {
            smith = new SmithWaterman(fileOutput, import.Sequences, GAP_COST, POSITIVE_GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapExt()
        {
            smith = new SmithWaterman(fileOutput, import.Sequences, GAP_COST, ZERO_GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Gap cost must be greater/equal to gap extension cost
        /// </summary>
        [TestMethod]
        public void GapCostNotGreaterThanExtCost()
        {
            smith = new SmithWaterman(fileOutput, import.Sequences, GAP_COST, HIGH_GAP_EXT,
                resolver, simMatrix);

            Assert.IsTrue(smith.Failed);
        }

        /// <summary>
        /// Normalised edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void NormalisedEdgesSet()
        {
            smith.CalculateScore();
            Assert.IsNotNull(smith.NormalisedEdges);
        }

        /// <summary>
        /// Edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void EdgesSet()
        {
            smith.CalculateScore();
            Assert.IsNotNull(smith.Edges);
        }
        #endregion
    }
}
