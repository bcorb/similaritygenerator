﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using Bio.SimilarityMatrices;
using Bio;
using System.Collections.Generic;
using System.Linq;

namespace SimilarityGeneratorTest
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the Nucmer.cs class
    /// </summary>
    [TestClass]
    public class ScoreNucmerTests
    {
        #region Class variables
        private Nucmer nucmer;
        private Import import;
        private SimilarityMatrix.StandardSimilarityMatrix simMatrix;
        private SimpleConsensusResolver resolver;
        private List<Edge> edges;
        #endregion

        #region Constants
        private const int GAP_COST = -1;
        private const int ZERO_GAP_COST = 0;
        private const int POSITIVE_GAP_COST = 10;
        private const int ZERO_GAP_EXT = 0;
        private const int POSITIVE_GAP_EXT = 10;
        private const int GAP_EXT = -1;
        private const int HIGH_GAP_EXT = 50;
        private const int RESOVLER = 2;
        private const int ZERO_MUM = 0;
        private const int NEGATIVE_MUM = -10;
        private const int LENGTH_MUM = 10;
        private const String proteinFileImport = "test.faa";
        private const String dnaFileImport = "DNAtest.faa";
        private const string fileOutput = "output";
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            import = new Import(dnaFileImport);
            import.LoadFromFile();

            simMatrix = SimilarityMatrix.StandardSimilarityMatrix.AmbiguousDna;
            resolver = new SimpleConsensusResolver(RESOVLER);

            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Exception thrown when protein file is imported
        /// </summary>
        [TestMethod]
        public void ProteinFileRead()
        {
            import = new Import(proteinFileImport);
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }


        /// <summary>
        /// Checks that DNA file is correctly identified
        /// </summary>
        [TestMethod]
        public void DnaFileRead()
        {
            nucmer.CalculateScore();
        }

        /// <summary>
        /// Confirms edges are created after file impoert
        /// </summary>
        [TestMethod]
        public void EdgesCreated()
        {
            nucmer.CalculateScore();
            Assert.IsNotNull(nucmer.Edges);
        }

        /// <summary>
        /// Edges are not created when file import is incorrect
        /// </summary>
        [TestMethod]
        public void EdgesNotCreated()
        {
            import = new Import(proteinFileImport);
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.AreEqual(nucmer.Edges.Count, 0);
        }

        /// <summary>
        /// Exception thrown when sequence count is zero
        /// </summary>
        [TestMethod]
        public void NoSequenceException()
        {
            string incorrectFile = "wrong";

            import = new Import(incorrectFile);
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Checks that similarity scores are added to each edge
        /// </summary>
        [TestMethod]
        public void ScoresGenerated()
        {
            nucmer.CalculateScore();

            edges = nucmer.Edges.ToList();

            Assert.IsNotNull(edges[0].Weight);
            Assert.IsNotNull(edges[1].Weight);
            Assert.IsNotNull(edges[3].Weight);
        }

        /// <summary>
        /// Checks that source is added to each edge
        /// </summary>
        [TestMethod]
        public void SourceAdded()
        {
            nucmer.CalculateScore();

            edges = nucmer.Edges.ToList();

            Assert.IsNotNull(edges[0].Source);
            Assert.IsNotNull(edges[1].Source);
            Assert.IsNotNull(edges[3].Source);
        }

        /// <summary>
        /// Checks that destination is added to each edge
        /// </summary>
        [TestMethod]
        public void DestinationAdded()
        {
            nucmer.CalculateScore();

            edges = nucmer.Edges.ToList();

            Assert.IsNotNull(edges[0].Destination);
            Assert.IsNotNull(edges[1].Destination);
            Assert.IsNotNull(edges[3].Destination);
        }

        /// <summary>
        /// No scores present for incorrect import
        /// </summary>
        [TestMethod]
        public void ScoresNotGenerated()
        {
            import = new Import(proteinFileImport);
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            nucmer.CalculateScore();
            Assert.AreEqual(0, nucmer.Edges.Count);
        }

        /// <summary>
        /// Exception is thrown when gap cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapCost()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, ZERO_GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapCost()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, POSITIVE_GAP_COST, GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is positive
        /// </summary>
        [TestMethod]
        public void PositiveGapExt()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, POSITIVE_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception is thrown when gap extension cost is zero
        /// </summary>
        [TestMethod]
        public void ZeroGapExt()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, ZERO_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Gap cost must be greater/equal to gap extension cost
        /// </summary>
        [TestMethod]
        public void GapCostNotGreaterThanExtCost()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, HIGH_GAP_EXT,
                resolver, simMatrix, LENGTH_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception thrown when mum lenght is zero
        /// </summary>
        [TestMethod]
        public void ZeroMumLength()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, ZERO_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception thrown when mum lenght is negative
        /// </summary>
        [TestMethod]
        public void NegativeMumLength()
        {
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, NEGATIVE_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Exception thrown when the similarity matrix alphabet
        /// does not match the input sequences
        /// </summary>
        [TestMethod]
        public void SimilarityMatrixNotMatchImportedAlphabet()
        {
            simMatrix = SimilarityMatrix.StandardSimilarityMatrix.AmbiguousRna;
            nucmer = new Nucmer(fileOutput, import.Sequences, GAP_COST, GAP_EXT,
                resolver, simMatrix, NEGATIVE_MUM);

            Assert.IsTrue(nucmer.Failed);
        }

        /// <summary>
        /// Normalised edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void NormalisedEdgesSet()
        {
            nucmer.CalculateScore();
            Assert.IsNotNull(nucmer.NormalisedEdges);
        }

        /// <summary>
        /// Edges property set after alignment performed
        /// </summary>
        [TestMethod]
        public void EdgesSet()
        {
            nucmer.CalculateScore();
            Assert.IsTrue(nucmer.Edges.Count > 0);
        }
        #endregion
    }
}
