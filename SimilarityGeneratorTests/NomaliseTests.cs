﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using System.Collections.Generic;
using Bio;

namespace SimilarityGeneratorTests
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the Normalise.cs class
    /// </summary>
    [TestClass]
    public class NomaliseTests
    {
        #region Class variables
        private Normalise norm;
        private List<Edge> edges;
        #endregion

        #region Constants
        private const double WEIGHT = 10;
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            edges = new List<Edge>();

            Edge edge1 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            Edge edge2 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            edges.Add(edge1);
            edges.Add(edge2);

            norm = new Normalise(edges);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Checks that score are generated on list
        /// </summary>
        [TestMethod]
        public void GetScoresNotNull()
        {
            Assert.IsNotNull(norm.GetNormalisedScores());
        }

        /// <summary>
        /// Checks that correct number of scores are generated
        /// </summary>
        [TestMethod]
        public void GetScoresCount()
        {
            Assert.AreEqual(edges.Count, norm.GetNormalisedScores().Count);
        }
        #endregion
    }
}
