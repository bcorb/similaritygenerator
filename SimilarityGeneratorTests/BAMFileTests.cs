﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bio;
using System.Collections.Generic;
using SimilarityGenerator.Model;

namespace SimilarityGeneratorTests
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Peforms unit tests for the BamFile.cs class
    /// </summary>
    [TestClass]
    public class BAMFileTests
    {
        #region Class variables
        private List<Edge> edges;
        private BamFile bam;
        #endregion

        #region Constants
        private const string FILE_LOCATION = "testfile";
        private const double WEIGHT = 10;
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            edges = new List<Edge>();

            Edge edge1 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            Edge edge2 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            edges.Add(edge1);
            edges.Add(edge2);

            bam = new BamFile(edges, FILE_LOCATION);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Creates a BAM file without exception
        /// </summary>
        [TestMethod]
        public void CreateBAMFile()
        {
            bam.OutputBamFile();
        }
        #endregion
    }
}
