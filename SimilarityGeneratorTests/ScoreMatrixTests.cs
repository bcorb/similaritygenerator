﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using System.Collections.Generic;
using Bio;

namespace SimilarityGeneratorTest
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the ScoreMatrix.cs class
    /// </summary>
    [TestClass]
    public class ScoreMatrixTests
    {
        #region Class variables
        private List<Edge> edges;
        private ScoreMatrix matrix;
        private ScoreMatrix matrix2;
        #endregion

        #region Constants
        private const string FILE_LOCATION = "testfile";
        private const int NUMBER_SEQ = 1;
        private const double WEIGHT = 10;
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            edges = new List<Edge>();

            Edge edge1 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            Edge edge2 = new Edge(new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  new Vertex(new Sequence(Alphabets.DNA, "AGCGTCCATG"), "test", 1),
                                  WEIGHT);

            edges.Add(edge1);
            edges.Add(edge2);

            matrix = new ScoreMatrix(NUMBER_SEQ, edges);
            matrix2 = new ScoreMatrix(NUMBER_SEQ, FILE_LOCATION, edges);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Checks whether the file exists when text file created
        /// </summary>
        [TestMethod]
        public void FileExistTxt()
        {
            matrix2.OutputTxtFile();
            Assert.IsTrue(matrix2.VerifyFileCreation);
        }

        /// <summary>
        /// No text file is created
        /// </summary>
        [TestMethod]
        public void FileDoesNotExistTxt()
        {
            Assert.IsFalse(matrix2.VerifyFileCreation);
        }

        /// <summary>
        /// Excel file created without exception
        /// </summary>
        [TestMethod]
        public void ExcelFileCreated()
        {
            matrix.OutputExcelFile();
        }

        /// <summary>
        /// Checks that graph edges population for
        /// text file constructor
        /// </summary>
        [TestMethod]
        public void GraphEdgesAddedText()
        {
            Assert.IsNotNull(matrix2.GraphEdges);
        }

        /// <summary>
        /// Checks that graph edges population for
        /// excel file constructor
        /// </summary>
        [TestMethod]
        public void GraphEdgesAddedExcel()
        {
            Assert.IsNotNull(matrix.GraphEdges);
        }
        #endregion
    }
}
