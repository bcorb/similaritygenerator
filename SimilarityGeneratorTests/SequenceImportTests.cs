﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;

namespace SimilarityGeneratorTest
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the Import.cs class
    /// </summary>
    [TestClass]
    public class SequenceImportTests
    {
        #region Class variables
        private String uri = "DNAtest.faa";
        private Import import;
        #endregion

        #region Constants
        private const int NUMBER_OF_SEQUENCES = 4;
        private const int NO_SEQUENCES = 0;
        #endregion

        #region Test setup
        [TestInitialize]
        public void SetUp()
        {
            import = new Import(uri);
        }
        #endregion

        #region Unit test cases
        /// <summary>
        /// Checks files import from test file
        /// </summary>
        [TestMethod]
        public void FileImportedCorrectly()
        {
            import.LoadFromFile();
            Assert.IsTrue(import.ImportSuccessful);
        }

        /// <summary>
        /// Validates that sequence list is created properly
        /// </summary>
        [TestMethod]
        public void SequenceListCreated()
        {
            Assert.IsNotNull(import.Sequences);
        }

        /// <summary>
        /// Checks that no sequences are generated from incorrect file
        /// </summary>
        [TestMethod]
        public void FileNotImportedCorrectly()
        {
            String incorrectUri = "wrong";
            Import wrongImport = new Import(incorrectUri);
            wrongImport.LoadFromFile();

            Assert.IsFalse(import.ImportSuccessful);
        }

        /// <summary>
        /// Checks whether sequence list contains values
        /// </summary>
        [TestMethod]
        public void SequenceListNotNull()
        {
            import.LoadFromFile();
            Assert.IsNotNull(import.Sequences);
        }

        /// <summary>
        /// Sequence count is correct
        /// </summary>
        [TestMethod]
        public void SequenceNumberCorrect()
        {
           import.LoadFromFile();
           Assert.AreEqual(NUMBER_OF_SEQUENCES, import.NumberSequences);
        }

        /// <summary>
        /// No sequence list created from incorrect file
        /// </summary>
        [TestMethod]
        public void SequenceCountFromIncorrectFile()
        {
            String incorrectUri = "wrong";
            Import wrongImport = new Import(incorrectUri);
            wrongImport.LoadFromFile();

            Assert.AreEqual(NO_SEQUENCES, import.Sequences.Count);
        }
        #endregion
    }
}
