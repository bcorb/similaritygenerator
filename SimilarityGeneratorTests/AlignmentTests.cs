﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimilarityGenerator.Model;
using System.Collections.Generic;
using Bio;

namespace SimilarityGeneratorTests
{
    /// <summary>
    /// Author: Brendan Corbett
    /// 
    /// Performs unit tests for the Alignment.cs class
    /// </summary>
    [TestClass]
    public class AlignmentTests
    {
        #region Class variables
        private Alignment align;
        private List<ISequence> sequences;
        #endregion

        #region Test setup
        /// <summary>
        /// Setups initial test class variables
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            sequences = new List<ISequence>();
            ISequence seqOne = new Sequence(Alphabets.DNA, 
                "ATGGCATATCCCATACAACTAGGATTCCAAGATGCAACATCACCAATCATAGAAGAACTACTTCACTTTCATGAC");
            ISequence seqTwo = new Sequence(Alphabets.DNA, 
                "TCAGAATTAAAGCCAGGGGAGCTACGACTATTAGAAGTCGATAATCGAGTTGTACTACCAATAGAAATAACAATC");
            sequences.Add(seqOne);
            sequences.Add(seqTwo);

            align = new Alignment();            
            align.FileLocation = "C:\\Users\\mac8\\Documents\\Visual Studio 2013\\Projects\\SimilarityGenerator2\\SimilarityGeneratorTests\\bin\\Debug";
            align.SequenceList = sequences;
        }
        #endregion

        #region Unit tests cases
        /// <summary>
        /// Peforms alignment for NeedlemanWunsch
        /// Checks that normalised scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentNeedleNormalised()
        {
            align.SelectedAligner = Alignment.Aligner.NeedlemanWunsch;
            align.PeformAlignment();

            Assert.IsNotNull(align.NormalisedEdges);
        }

        /// <summary>
        /// Peforms alignment for NeedlemanWunsch
        /// Checks that weight scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentNeedleScores()
        {
            align.SelectedAligner = Alignment.Aligner.NeedlemanWunsch;
            align.PeformAlignment();

            Assert.IsNotNull(align.Scores);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that normalised scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentSmithNormalised()
        {
            align.SelectedAligner = Alignment.Aligner.SmithWaterman;
            align.PeformAlignment();

            Assert.IsNotNull(align.NormalisedEdges);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that weight scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentSmithScores()
        {
            align.SelectedAligner = Alignment.Aligner.SmithWaterman;
            align.PeformAlignment();

            Assert.IsNotNull(align.Scores);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that normalised scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentMumNormalised()
        {
            align.SelectedAligner = Alignment.Aligner.Mummer;
            align.PeformAlignment();

            Assert.IsNotNull(align.NormalisedEdges);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that weight scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentMumScores()
        {
            align.SelectedAligner = Alignment.Aligner.Mummer;
            align.PeformAlignment();

            Assert.IsNotNull(align.Scores);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that normalised scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentNucmerNormalised()
        {
            align.SelectedAligner = Alignment.Aligner.Nucmer;
            align.PeformAlignment();

            Assert.IsNotNull(align.NormalisedEdges);
        }

        /// <summary>
        /// Peforms alignment for SmithWaterman
        /// Checks that weight scores are present
        /// </summary>
        [TestMethod]
        public void PerformAlignmentNucmerScores()
        {
            align.SelectedAligner = Alignment.Aligner.Nucmer;
            align.PeformAlignment();

            Assert.IsNotNull(align.Scores);
        }

        /// <summary>
        /// Mummer field set to shown when mummer selected
        /// </summary>
        [TestMethod]
        public void MummerShown()
        {
            align.SelectedAligner = Alignment.Aligner.Mummer;
            Assert.IsTrue(align.MummerFields);
        }

        /// <summary>
        /// Mummer field set to hide when mummer not selected
        /// </summary>
        [TestMethod]
        public void MummerHidden()
        {
            align.SelectedAligner = Alignment.Aligner.NeedlemanWunsch;
            Assert.IsFalse(align.MummerFields);
        }
        #endregion
    }
}
